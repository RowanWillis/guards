﻿using Xunit;

namespace RowanWillis.Guards.UnitTests;

public class ComparableGuardTests
{
    [Theory]
    [InlineData(100, 50)]
    public void IsGreaterThan_GivenALesserValue_ReturnsTheGivenGuard(int value, int limit)
    {
        var guard = Ensure.That(value).IsGreaterThan(limit);
        Assert.Equal(value, guard);
    }
    
    [Theory]
    [InlineData(1, 1)]
    public void IsGreaterThan_GivenAComparableValue_ThrowsArgumentException(int value, int limit)
    {
        var exception = Assert.Throws<ArgumentException>(() => Ensure.That(value).IsGreaterThan(limit));
        Assert.Contains($"must be greater than {limit}", exception.Message);
    }
    
    [Theory]
    [InlineData(-500, 17)]
    public void IsGreaterThan_GivenAGreaterValue_ThrowsArgumentException(int value, int limit)
    {
        var exception = Assert.Throws<ArgumentException>(() => Ensure.That(value).IsGreaterThan(limit));
        Assert.Contains($"must be greater than {limit}", exception.Message);
    }
    
    
    [Theory]
    [InlineData(50, 100)]
    public void IsNoGreaterThanThan_GivenAGreaterValue_ReturnsTheGivenGuard(int value, int limit)
    {
        var guard = Ensure.That(value).IsNotGreaterThan(limit);
        Assert.Equal(value, guard);
    }
    
    [Theory]
    [InlineData(1, 1)]
    public void IsNoGreaterThan_GivenAComparableValue_ReturnsTheGivenGuard(int value, int limit)
    {
        var guard = Ensure.That(value).IsNotGreaterThan(limit);
        Assert.Equal(value, guard);
    }
    
    [Theory]
    [InlineData(17, -500)]
    public void IsNoGreaterThan_GivenALesserValue_ThrowsArgumentException(int value, int limit)
    {
        var exception = Assert.Throws<ArgumentException>(() => Ensure.That(value).IsNotGreaterThan(limit));
        Assert.Contains($"must not be greater than {limit}", exception.Message);
    }
    
    [Theory]
    [InlineData(50, 100)]
    public void IsLessThan_GivenAGreaterValue_ReturnsTheGivenGuard(int value, int limit)
    {
        var guard = Ensure.That(value).IsLessThan(limit);
        Assert.Equal(value, guard);
    }
    
    [Theory]
    [InlineData(1, 1)]
    public void IsLessThan_GivenAComparableValue_ThrowsArgumentException(int value, int limit)
    {
        var exception = Assert.Throws<ArgumentException>(() => Ensure.That(value).IsLessThan(limit));
        Assert.Contains($"must be less than {limit}", exception.Message);
    }
    
    [Theory]
    [InlineData(17, -500)]
    public void IsLessThan_GivenALesserValue_ThrowsArgumentException(int value, int limit)
    {
        var exception = Assert.Throws<ArgumentException>(() => Ensure.That(value).IsLessThan(limit));
        Assert.Contains($"must be less than {limit}", exception.Message);
    }
    
    [Theory]
    [InlineData(100, 50)]
    public void IsNotLessThan_GivenALesserValue_ReturnsTheGivenGuard(int value, int limit)
    {
        var guard = Ensure.That(value).IsNotLessThan(limit);
        Assert.Equal(value, guard);
    }
    
    [Theory]
    [InlineData(1, 1)]
    public void IsNotLessThan_GivenAComparableValue_ReturnsTheGivenGuard(int value, int limit)
    {
        var guard = Ensure.That(value).IsNotLessThan(limit);
        Assert.Equal(value, guard);
    }
    
    [Theory]
    [InlineData(-500, 17)]
    public void IsNotLessThan_GivenAGreaterValue_ThrowsArgumentException(int value, int limit)
    {
        var exception = Assert.Throws<ArgumentException>(() => Ensure.That(value).IsNotLessThan(limit));
        Assert.Contains($"must not be less than {limit}", exception.Message);
    }
}