﻿using Xunit;

namespace RowanWillis.Guards.UnitTests;

public class ClassGuardTests
{
    internal class StubClass { }

    [Fact]
    public void GivenNull_EnusureNotNullThrowsArgumentException()
    {
#nullable disable
        Assert.Throws<ArgumentNullException>(() => Ensure.That((StubClass)null).IsNotNull());
#nullable enable
    }
}