﻿using Xunit;

namespace RowanWillis.Guards.UnitTests;

public class BaseGuardTests
{
    [Theory]
    [InlineData("my string")]
    [InlineData(100)]
    [InlineData(true)]
    public void ANewGuardHasTheGivenValue<T>(T value)
    {
        var guard = Ensure.That(value);

        Assert.Equal(value, guard.Value);
        Assert.Equal(value, guard);
    }

    [Theory]
    [InlineData("my string", "AString")]
    [InlineData(100, "AnInteger")]
    [InlineData(true, "ABoolean")]
    public void ANewGuardHasTheGivenName<T>(T value, string name)
    {
        var guard = Ensure.That(value, name);

        Assert.Equal(name, guard.Name);
    }

    [Theory]
    [InlineData("my string")]
    [InlineData(100)]
    [InlineData(true)]
    public void WhenNoNameIsGiven_AndTheGuardedValueIsANamedVariable_TheGuardsNameIsTheNameOfTheGuardedVariable<T>(T valueToGuard)
    {
        var guard = Ensure.That(valueToGuard);

        Assert.Equal(nameof(valueToGuard), guard.Name);
    }

    [Fact]
    public void WhenNoNameIsGiven_AndTheGuardedValueIsAnInlineExpression_TheGuardsNameIsTheStringifiedExpression()
    {
        var guard = Ensure.That("something".Length);

        Assert.Equal("\"something\".Length", guard.Name);
    }

    [Fact]
    public void WhenNullNameIsGiven_ThenTheNameIsValue()
    {
        var guard = Ensure.That("something", null);
        Assert.Equal("Value", guard.Name);
    }
}