﻿using Xunit;

namespace RowanWillis.Guards.UnitTests.Strings;

public partial class StringGuardTests
{
    public static TheoryData<string> NamesOnly = new()
    {
        { "Name" },
        { "Email" },
        { "Sentence" }
    };

    public static TheoryData<string, string> WhitespaceStrings = new()
    {
        { "OneSpace", " " },
        { "ManySpaces", "           " },
        { "OtherWhitespaces", "\t \r\n" }
    };

    public static TheoryData<string, string> TypicalStrings = new()
    {
        { "Name", "Bob" },
        { "Email", "my.email@test.com" },
        { "Sentence", "The quick brown fox jumps over the lazy dog." }
    };

    private static string Repeat(char character, int times) => new(character, times);
}