﻿using RowanWillis.Guards.Strings;
using Xunit;

namespace RowanWillis.Guards.UnitTests.Strings;

public partial class StringGuardTests
{
    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void HasNoLeadingWhitespace_GivenNull_ReturnsTheGivenGuard(string name)
    {
#nullable disable
        var guard = Ensure.That((string)null, name).HasNoLeadingWhitespace();
#nullable enable

        Assert.Null(guard.Value);
    }

    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void HasNoLeadingWhitespace_GivenEmptyString_ReturnsTheGivenGuard(string name)
    {
        var guard = Ensure.That("", name).HasNoLeadingWhitespace();
        Assert.NotNull(guard.Value);
        Assert.Empty(guard.Value);
    }

    [Theory]
    [MemberData(nameof(WhitespaceStrings))]
    public void HasNoLeadingWhitespace_GivenOnlyWhitespace_ThrowsArgumentExcpetion_DescribingTheProblem(string name, string? value)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That(value, name).HasNoLeadingWhitespace());

        Assert.Contains($"{name} must not have leading whitespace.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(WhitespaceStrings))]
    public void HasNoLeadingWhitespace_GivenLeadingWhitespaceWhitespace_ThrowsArgumentExcpetion_DescribingTheProblem(string name, string whitespace)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That($"{whitespace}non-whitespace", name).HasNoLeadingWhitespace());

        Assert.Contains($"{name} must not have leading whitespace.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(TypicalStrings))]
    public void HasNoLeadingWhitespace_GivenNoLeadingWhitespaceWhitespace_ReturnsTheGivenGuard(string name, string value)
    {
        var guard = Ensure.That(value, name).HasNoLeadingWhitespace();

        Assert.Equal(value, guard);
    }
}