﻿using RowanWillis.Guards.Strings;
using Xunit;

namespace RowanWillis.Guards.UnitTests.Strings;

public partial class StringGuardTests
{
    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void IsNotNullOrEmpty_GivenNull_ThrowsArgumentException_DescribingTheProblem(string name)
    {
#nullable disable
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That((string)null, name).IsNotNullOrEmpty());
#nullable enable

        Assert.Contains($"{name} must not be null or empty.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void IsNotNullOrEmpty_GivenEmptyString_ThrowsArgumentException_DescribingTheProblem(string name)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That("", name).IsNotNullOrEmpty());

        Assert.Contains($"{name} must not be null or empty", exception.Message);
    }

    [Theory]
    [MemberData(nameof(TypicalStrings))]
    [MemberData(nameof(WhitespaceStrings))]
    public void IsNotNullOrEmpty_GivenANonEmptyString_ReturnsTheGivenGuard(string name, string value)
    {
        var guard = Ensure.That(value, name).IsNotNullOrEmpty();

        Assert.Equal(value, guard);
    }
}