﻿using RowanWillis.Guards.Strings;
using Xunit;

namespace RowanWillis.Guards.UnitTests.Strings;

public partial class StringGuardTests
{
    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void HasNoTrailingWhitespace_GivenNull_ReturnsTheGivenGuard(string name)
    {
#nullable disable
        var guard = Ensure.That((string)null, name).HasNoTrailingWhitespace();
#nullable enable

        Assert.Null(guard.Value);
    }

    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void HasNoTrailingWhitespace_GivenEmptyString_ReturnsTheGivenGuard(string name)
    {
#nullable disable
        var guard = Ensure.That("", name).HasNoTrailingWhitespace();
#nullable enable

        Assert.NotNull(guard.Value);
        Assert.Empty(guard.Value);
    }

    [Theory]
    [MemberData(nameof(WhitespaceStrings))]
    public void HasNoTrailingWhitespace_GivenOnlyWhitespace_ThrowsArgumentExcpetion_DescribingTheProblem(string name, string value)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That(value, name).HasNoTrailingWhitespace());

        Assert.Contains($"{name} must not have trailing whitespace.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(WhitespaceStrings))]
    public void HasNoTrailingWhitespace_GivenTrailingWhitespaceWhitespace_ThrowsArgumentExcpetion_DescribingTheProblem(string name, string whitespace)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That($"non-whitespace{whitespace}", name).HasNoTrailingWhitespace());

        Assert.Contains($"{name} must not have trailing whitespace.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(TypicalStrings))]
    public void HasNoTrailingWhitespace_GivenNoTrailingWhitespaceWhitespace_ReturnsTheGivenGuard(string name, string value)
    {
        var guard = Ensure.That(value, name).HasNoTrailingWhitespace();

        Assert.Equal(value, guard);
    }
}