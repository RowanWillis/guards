﻿using RowanWillis.Guards.Strings;
using Xunit;

namespace RowanWillis.Guards.UnitTests.Strings;

public partial class StringGuardTests
{
    public static TheoryData<string, string, int> LongEnoughStrings = new()
    {
        { "LongString", Repeat('.', 55), 30 },
        { "LongString", "", 0 }
    };

    public static TheoryData<string, string, int> TooShortStrings = new()
    {
        { "LongString", Repeat('x', 19), 20 },
        { "LongString", Repeat('a', 99), 100 }
    };

    [Theory]
    [InlineData(0)]
    [InlineData(15)]
    [InlineData(50)]
    [InlineData(100)]
    [InlineData(9999)]
    public void HasLengthAtLeast_GiveNull_ReturnsNull(int maxLength)
    {
        var guard = Ensure.That((string?)null).HasLengthAtLeast(maxLength);
        Assert.Null(guard.Value);
    }

    [Theory]
    [MemberData(nameof(TooShortStrings))]
    public void HasLengthAtLeast_GivenAStringThatIsTooShort_ThrowsArgumentExcpetion_DescribingTheLengthViolation(
        string name,
        string? value,
        int maxLength)
    {
        var exception = Assert.Throws<ArgumentException>(() => Ensure.That(value, name).HasLengthAtLeast(maxLength));
        Assert.Contains($"{name} must have length at least {maxLength}.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(LongEnoughStrings))]
    public void HasLengthAtLeast_GivenAStringThatIsNotTooShort_ReturnsTheGuard(string name, string? value, int maxLength)
    {
        var guard = Ensure.That(value, name).HasLengthAtLeast(maxLength);
        Assert.Equal(value, guard);
    }
}
