﻿using RowanWillis.Guards.Strings;
using Xunit;

namespace RowanWillis.Guards.UnitTests.Strings;
public partial class StringGuardTests
{
    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void IsNotNullOrWhitespace_GivenNull_ThrowsArgumentException_DescribingTheProblem(string name)
    {
#nullable disable
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That((string)null, name).IsNotNullOrWhitespace());
#nullable enable

        Assert.Contains($"{name} must not be null or whitespace.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(NamesOnly))]
    public void IsNotNullOrWhitespace_GivenEmptyString_ThrowsArgumentException_DescribingTheProblem(string name)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That("", name).IsNotNullOrWhitespace());

        Assert.Contains($"{name} must not be null or whitespace.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(WhitespaceStrings))]
    public void IsNotNullOrWhitespace_GivenWhitespace_ThrowsArgumentException_DescribingTheProblem(string name, string value)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That(value, name).IsNotNullOrWhitespace());

        Assert.Contains($"{name} must not be null or whitespace.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(TypicalStrings))]
    public void IsNotNullOrWhitespace_GivenNonEmptyNonWhitespaceString_ReturnsTheGivenGuard(string name, string value)
    {
        var guard = Ensure.That(value, name).IsNotNullOrWhitespace();

        Assert.Equal(value, guard);
    }
}