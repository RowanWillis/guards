﻿using RowanWillis.Guards.Strings;
using Xunit;

namespace RowanWillis.Guards.UnitTests.Strings;

public partial class StringGuardTests
{
    public static TheoryData<string, string, int> ShortEnoughStrings = new()
    {
        { "LongString", Repeat('.', 19), 30 },
        { "LongString", Repeat('x', 50), 50 },
        { "LongString", "", 100 }
    };

    public static TheoryData<string, string, int> TooLongStrings = new()
    {
        { "LongString", Repeat('x', 21), 20 },
        { "LongString", Repeat('a', 100), 50 }
    };

    [Theory]
    [InlineData(0)]
    [InlineData(15)]
    [InlineData(50)]
    [InlineData(100)]
    [InlineData(9999)]
    public void HasLengthAtMost_GivenNull_ReturnsNull(int maxLength)
    {
#nullable disable
        var guard = Ensure.That((string)null).HasLengthAtMost(maxLength);
#nullable enable

        Assert.Null(guard.Value);
    }

    [Theory]
    [MemberData(nameof(TooLongStrings))]
    public void HasLengthAtMost_GivenAStringThatIsTooLong_ThrowsArgumentExcpetion_DescribingTheLengthViolation(
        string name,
        string value,
        int maxLength)
    {
        var exception = Assert.Throws<ArgumentException>(
            () => Ensure.That(value, name).HasLengthAtMost(maxLength));

        Assert.Contains($"{name} must have length at most {maxLength}.", exception.Message);
    }

    [Theory]
    [MemberData(nameof(ShortEnoughStrings))]
    public void HasLengthAtMost_GivenAStringThatIsNotTooLong_ReturnsTheGuard(
        string name,
        string value,
        int maxLength)
    {
        var guard = Ensure.That(value, name).HasLengthAtMost(maxLength);

        Assert.Equal(value, guard);
    }
}