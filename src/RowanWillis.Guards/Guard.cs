﻿namespace RowanWillis.Guards;

public class Guard<T>
{
    internal Guard(T value, string? name = null)
    {
        Value = value;
        Name = name ?? "Value";
    }

    public T Value { get; }

    public string Name { get; }

    public static implicit operator T(Guard<T> guard)
        => guard.Value;
}