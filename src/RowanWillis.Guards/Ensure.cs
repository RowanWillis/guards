﻿using System.Runtime.CompilerServices;

namespace RowanWillis.Guards;

public static class Ensure
{
    public static Guard<T> That<T>(
        T value,
        [CallerArgumentExpression("value")] string? name = null)
        => new(value, name);
}