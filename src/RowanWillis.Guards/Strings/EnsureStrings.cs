﻿namespace RowanWillis.Guards.Strings;

public static class EnsureString
{
    public static Guard<string?> IsNotNullOrEmpty(this Guard<string?> guard)
    {
        if (string.IsNullOrEmpty(guard.Value))
            throw new ArgumentException($"{guard.Name} must not be null or empty.", guard.Name);

        return guard;
    }

    public static Guard<string?> IsNotNullOrWhitespace(this Guard<string?> guard)
    {
        if (string.IsNullOrWhiteSpace(guard.Value))
            throw new ArgumentException($"{guard.Name} must not be null or whitespace.", guard.Name);

        return guard;
    }

    public static Guard<string?> HasNoLeadingWhitespace(this Guard<string?> guard)
    {
        if (guard.Value is null || guard.Value.Length == 0)
            return guard;

        if (char.IsWhiteSpace(guard.Value[0]))
            throw new ArgumentException($"{guard.Name} must not have leading whitespace.", guard.Name);

        return guard;
    }

    public static Guard<string?> HasNoTrailingWhitespace(this Guard<string?> guard)
    {
        if (guard.Value is null || guard.Value.Length == 0)
            return guard;

        if (char.IsWhiteSpace(guard.Value[^1]))
            throw new ArgumentException($"{guard.Name} must not have trailing whitespace.", guard.Name);

        return guard;
    }

    public static Guard<string?> HasNoOuterWhitespace(this Guard<string?> guard)
        => guard
            .HasNoLeadingWhitespace()
            .HasNoTrailingWhitespace();

    public static Guard<string?> HasLengthAtLeast(this Guard<string?> guard, int minLength)
    {
        if (guard.Value is null)
            return guard;

        if (guard.Value.Length < minLength)
            throw new ArgumentException($"{guard.Name} must have length at least {minLength}.", guard.Name);

        return guard;
    }

    public static Guard<string?> HasLengthAtMost(this Guard<string?> guard, int maxLength)
    {
        if (guard.Value is null)
            return guard;

        if (guard.Value.Length > maxLength)
            throw new ArgumentException($"{guard.Name} must have length at most {maxLength}.", guard.Name);

        return guard;
    }

    public static Guard<string?> HasLengthBetween(this Guard<string?> guard, int minLength, int maxLength)
        => guard
            .HasLengthAtLeast(minLength)
            .HasLengthAtMost(maxLength);
}