﻿namespace RowanWillis.Guards;

public static class EnsureComparable
{
    public static Guard<T> IsGreaterThan<T>(this Guard<T> guard, T value) where T : IComparable<T>
    {
        if (guard.Value.CompareTo(value) <= 0)
            throw new ArgumentException($"{guard.Name} must be greater than {value}", guard.Name);

        return guard;
    }
    
    public static Guard<T> IsLessThan<T>(this Guard<T> guard, T value) where T : IComparable<T>
    {
        if (guard.Value.CompareTo(value) >= 0)
            throw new ArgumentException($"{guard.Name} must be less than {value}", guard.Name);

        return guard;
    }
    
    public static Guard<T> IsNotGreaterThan<T>(this Guard<T> guard, T value) where T : IComparable<T>
    {
        if (guard.Value.CompareTo(value) > 0)
            throw new ArgumentException($"{guard.Name} must not be greater than {value}", guard.Name);

        return guard;
    }
    
    public static Guard<T> IsNotLessThan<T>(this Guard<T> guard, T value) where T : IComparable<T>
    {
        if (guard.Value.CompareTo(value) < 0)
            throw new ArgumentException($"{guard.Name} must not be less than {value}", guard.Name);

        return guard;
    }
}