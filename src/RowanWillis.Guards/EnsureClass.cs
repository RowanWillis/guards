﻿namespace RowanWillis.Guards;

public static class EnsureClass
{
    public static Guard<T> IsNotNull<T>(this Guard<T> guard)
        where T : class
    {
        if (guard.Value is null)
            throw new ArgumentNullException(guard.Name);

        return guard;
    }
}