﻿# Guards

This library provides an extensible fluent interface for ensuring that variables meet given criteria.

## Usage

It is quite common to write guard clauses at the beginning of methods which throw an exception if a parameter is somehow invalid. For instance:

```csharp
public void Print(string thingToPrint)
{
    if (string.IsNullOrEmpty(thingToPrint))
        throw new ArgumentException($"{nameof(thingToPrint)} must not be null or empty.", nameof(thingToPrint));
    
    Console.WriteLine(thingToPrint);
}
```

The aim of this library is to reduce the code footprint of these checks and increase readability.

```csharp
public void Print(string thingToPrint)
{
    Ensure.That(thingToPrint)
        .IsNotNull()
        .IsNotEmpty();
        
    Console.WriteLine(thingToPrint);
}
```

As above, checks can be chained together in a fluent manner. The first check to fail will throw an exception, with the argument name automatically inferred in the case of `ArgumentException` or `ArgumentNullException`.

If desired, the reported argument name can be overidden by providing an optional second parameter to the `Ensure.That` method:

```csharp
public void Print(string thingToPrint)
{
    Ensure.That(thingToPrint, "myCustomName")
        .IsNotNull()
        .IsNotEmpty();
        
    Console.WriteLine(thingToPrint);
}
```

The above would throw an exception with the argument name set to `myCustomName`, which would also be used in the exception message.

### Supported Types

At present, this library contains methods for checking:
* Nullability of objects,
* Emptiness, whitespace content and length of strings,
* Comparison of `IComparable` values.

It is intended to keep the built-in support relatively lean since hte API is easily extensible.

_Note:_ In general, the checks are designed to be logically independent where possible. For instance `IsNotEmpty()` will _not_ fail when given null, to allow the possibility that null value is valid even if an empty value is not. If neither case is valid, the two methods can be chained as in the example above.

## Extending

Checks for other types can easily be added by defining extension methods on the `Guard<T>` type, where the generic parameter `T` is the type of the object being checked.

For instance, suppose we were given a `HttpClient` and we want to ensure it is not null, and also to specifically throw an `InvalidOperationException` if its `BaseAddress` property has not been set.

We could write:

```csharp
public void Method(HttpClient httpClient)
{
    Ensure.That(httpClient)
        .IsNotNull();
        
    Ensure.That(httpClient.BaseAddress)
        .IsNotNull()
        .IsNotEmpty();
        
    //...
}
```

However, this would throw an `ArgumentNullException`, reporting that `httpClient.BaseAddress` must not be null. Instead we can define the following: 

```csharp
public static Guard<HttpClient> HasBaseAddress(this Guard<HttpClient> guard)
{
    if (string.IsNullOrEmpty(guard.Value.BaseAddress))
        throw new InvalidOperationException($"Base address must be set on {guard.Name}");
        
    return guard;
}
```

Here, the extension method takes a `Guard<HttpClient>` argument and accesses the argument value through `guard.Value`. It then uses `guard.Name` to report the correct variable name. Finally, it returns the guard for chaining.

We could then use this as follows:

```csharp
public void Method(HttpClient httpClient)
{
    Ensure.That(httpClient)
        .IsNotNull()
        .HasBaseAddress();
        
    //...
}
```